<?php
namespace ChessPos;


class BoardImage {
    protected $board;
    protected $image;
    protected $pieceDir  = '/share/pieces/new/64/';
    protected $pieceSize = 64;

    public function __construct($board) {
        $this->board = $board;
    }

    public function outputImage($rotate = false) {
        $image = $this->generate($rotate);

        // header('Content-Type: image/gif');
        return imagegif($image);
    }

    public function saveImage($filePath) {
        $image = $this->generate();
        imagegif($image, $filePath);
    }

    public function generate($rotate = false) {
        if (empty($this->image)) {
            $this->image = $this->_generateImage($rotate);
        }
        if($rotate) {
            $this->image = imagerotate($this->image, 180, 0);
        }

        return $this->image;
    }


    protected function _generateImage($rotate = false) {
        $boardImage = \imagecreate(600, 600)
                    or die("Cannot Instantiate new GD image stream");
        $background = \imagecolorallocate($boardImage, 0, 0, 0);
        $board = $this->board->getBoard();

        $numberImage = BASE_DIR . "{$this->pieceDir}numbers.gif";
        $numberImage = \imagecreatefromgif($numberImage);
        \imagecopy($boardImage, $numberImage, 0, 0, 0, 0, 600, 600);

        for ($square=0; $square < 64; $square++) {
            $piece      = $board[$square];
            $pieceImage = $this->_getPieceImage($piece);
            $offsetX    = ($square % 8) * $this->pieceSize;
            $offsetY    = floor($square / 8) * $this->pieceSize;
            if($rotate) {
                $pieceImage = imagerotate($pieceImage, 180, 0);
            }

            \imagecopy($boardImage, $pieceImage, $offsetX + 44, $offsetY + 44, 0, 0, $this->pieceSize, $this->pieceSize);
        }

        return $boardImage;
    }

    protected function _getPieceImage($piece) {
        if (empty($this->imageCache[$piece])) {
            $pieceImage = BASE_DIR . "{$this->pieceDir}{$piece}.gif";
            $image = \imagecreatefromgif($pieceImage);
            $this->imageCache[$piece] = $image;
        }

        return $this->imageCache[$piece];
    }
}

?>
