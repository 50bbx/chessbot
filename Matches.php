<?php
require_once 'medoo.php';
require_once 'ChessObj/Pear.php';
require_once 'GifCreator/GifCreator.php';

class Matches
{
    public function __construct($chatId)
    {
        $this->database = new medoo([
            'database_type' => 'mysql',
            'database_name' => 'chessbot',
            'server' => '10.130.63.101:3306',
            'username' => 'adminBqHui4Z',
            'password' => 'xlBvuKlBCz1E',
            'charset' => 'utf8'
        ]);

        $result = $this->database->select('matches', '*', [
            "chat_id[=]" => $chatId
        ]);

        if(count($result) == 1){
            $this->id = $result[0]['id'];
            $this->chatId = $result[0]['chat_id'];
            $this->state = unserialize($result[0]['state']);
            $this->turn = $result[0]['turn'];
        } else {
            $this->chatId = $chatId;
            $this->state = new Chess\Game\ChessGame();
            $this->state->resetGame();
        }
    }

    public function save()
    {
        if(isset($this->id)) {
            $this->database->update('matches', [
                'chat_id' => $this->chatId,
                'state' => serialize($this->state),
                'turn' => $this->turn,
            ], [
                "id" => $this->id
            ]);
        } else {
            $this->database->insert('matches', [
                'chat_id' => $this->chatId,
                'state' => serialize($this->state),
                'turn' => $this->getTurn(),
            ]);
        }
    }

    public function getBoardImage()
    {
        $arr = explode(' ', $this->state->renderFen());
        $posImage = str_replace('/', '-', $arr[0]);
        $fen = new ChessPos\FenBoard($posImage);
        $boardImage = new ChessPos\BoardImage($fen);
        return $boardImage;
    }

    public function move($san){
        return $this->state->moveSAN($san);
    }

    public function delete()
    {
        $this->database->delete('matches', [
            'chat_id[=]' => $this->chatId
        ]);
    }

    public function getTurn()
    {
        if(isset($this->turn)){
            return $this->turn;
        } else {
            return '';
        }
    }

    public function undo()
    {
        $couples = $this->state->getMoveList();
        error_log(serialize($couples));
        $this->state->resetGame();
        $coupleCount = count($couples);
        error_log('There are ' . $coupleCount . ' couples');

        //Scorro tutte le coppie.
        foreach($couples as $index => $couple){

            //Le coppie di mosse possono avere uno o due elementi (numero di mosse pari o dispari)
            $moveCount = count($couple);
            error_log('The couple has ' . $moveCount . ' move(s)');

            //Per ciascuno degli elementi della coppia (2 o, in fondo, 1)
            foreach($couple as $key => $move) {
                error_log($index . ': ' . $move);

                //Se non sono nell'ultima coppia
                if($coupleCount != $index) {

                    //muovo il pezzo
                    $validMove = $this->move($move);

                    //Se qualcosa è andato storto
                    if($validMove !== true){
                        error_log($validMove->message);
                    }
                } else {
                    error_log('Last couple');
                    if($moveCount > 1){
                        error_log('Last couple has 2 elements.');
                        if($key === 0) {
                            error_log('The first is executed.');
                            $validMove = $this->move($move);
                            if($validMove !== true){
                                error_log($validMove->message);
                            }
                        }
                    } else {
                        error_log('The couple has 1 elements. I will do nothing.');
                        // $validMove = $this->move($move);
                        // if($validMove !== true){
                        //     error_log($validMove->message);
                        // }
                    }
                }
            }
        }
    }

    public function getHistory()
    {
        $couples = $this->state->getMoveList(true);
        $list = "This is structured like a PGN file. Copy from below and paste it here http://chesstempo.com/pgn-viewer.html to replay your match!\n\n";
        foreach($couples as $index => $couple){
            $list .= $index . '. ';
            foreach($couple as $move) {
                $list .= $move . " ";
            }
        }

        return trim($list);
    }

    public function generateGif()
    {
        $couples = $this->state->getMoveList();
        $dummyMatch = new Chess\Game\ChessGame();
        $dummyMatch->resetGame();
        $frames = [];
        $durations = [];
        $arr = explode(' ', $dummyMatch->renderFen());
        $posImage = str_replace('/', '-', $arr[0]);
        $fen = new ChessPos\FenBoard($posImage);
        $boardImage = new ChessPos\BoardImage($fen);
        $frames[] = $boardImage->generate();
        $durations[] = 100;
        foreach($couples as $index => $couple){
            foreach($couple as $key => $move) {
                $dummyMatch->moveSAN($move);
                $arr = explode(' ', $dummyMatch->renderFen());
                $posImage = str_replace('/', '-', $arr[0]);
                $fen = new ChessPos\FenBoard($posImage);
                $boardImage = new ChessPos\BoardImage($fen);
                $frames[] = $boardImage->generate();
                $durations[] = 100;
            }
        }
        $gc = new GifCreator\GifCreator();
        $gc->create($frames, $durations, 0);
        return $gifBinary = $gc->getGif();
    }
}