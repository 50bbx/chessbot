<?php
define('DOC_ROOT', dirname(__FILE__) . 'chess.dev');
define('BASE_DIR', dirname(__FILE__));

require_once 'ChessPos/FenBoard.php';
require_once 'ChessPos/BoardImage.php';
require_once 'ChessObj/ChessGame.php';
$chess = new Chess\Game\ChessGame();

$validMove = $chess->moveSAN('Pf3');
if($validMove !== true) {
    echo $validMove->message;
    exit;
}
$arr = explode(' ', $chess->renderFen());
$posImage = str_replace('/', '-', $arr[0]);
$fen = new ChessPos\FenBoard($posImage);
$boardImage = new ChessPos\BoardImage($fen);
// Return dynamic image
ob_start();
$boardImage->outputImage();
$image_data = ob_get_contents();
ob_end_clean();

sendImage($image_data);
// And cache it on the filesystem
$filePath = BASE_DIR . "/board/{$posImage}";
$boardImage->saveImage($filePath);