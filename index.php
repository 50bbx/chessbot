<?php
define('DOC_ROOT', dirname(__FILE__));
define('BASE_DIR', dirname(__FILE__));

require_once 'ChessPos/FenBoard.php';
require_once 'ChessPos/BoardImage.php';
require_once 'ChessObj/ChessGame.php';
require_once 'Chats.php';

date_default_timezone_set('Europe/Rome');
define('TOKEN', '199700843:AAERVh78BdCaAlIPjy9ApV-2SVr7CvqOI40');
define('BASE_URL', 'https://api.telegram.org/bot' . TOKEN . '/');

/**
 * @property  message
 */
class WebhookHandler
{
    public function get() {
        $JSON = file_get_contents('php://input');
        if(!$JSON){
            echo "NO INPUT JSON";
            return false;
        }
        $respObj = json_decode($JSON);
        $this->update_id = $respObj->update_id;
        $this->message = $respObj->message;
        $this->message_id = $this->message->message_id;
        $this->date = $this->message->date;
        $this->text = @ucfirst($this->message->text);
        $this->user_id = $this->message->from->id;
        $this->chat_id = $this->message->chat->id;
        $this->chatType = $this->message->chat->type;
        $this->caption = '';
        $this->rotate = false;

        $this->chat = new Chats($this->chat_id);
        return true;
    }

    public function reply($message){
        $data = [
            'chat_id'=> $this->chat_id,
            'text'=> $message,
            'disable_web_page_preview'=> true,
            // 'reply_to_message_id': $message_id,
        ];
        $postData = http_build_query($data);
        $opts = array('http' =>
            array(
                'method'  => 'POST',
                'header'  => 'Content-type: application/x-www-form-urlencoded',
                'content' => $postData
            )
        );
        $postParams = stream_context_create($opts);
        $r = file_get_contents(BASE_URL . 'sendMessage', false, $postParams);
        error_log($r);
        exit;
    }

    public function checkMsg()
    {
        if(!$this->text){
            return false;
        }

        error_log(serialize($this->message));
        if(substr($this->text, 0, 1) === '/' ) {
            error_log('Comando');
            $this->executeCommand();
        }

        if(!$this->chat->active){
            return false;
        }

        if(!$this->chat->isGameOn()) {
            if(strtolower($this->text) === 'me' || strtolower($this->text) === 'daje') {
                if($this->chat->userIdA != $this->user_id){
                    error_log('Setting other player...');
                    $this->chat->userIdB = $this->user_id;
                    $this->chat->match->turn = $this->chat->userIdA;
                    $this->chat->match->save();
                    $this->chat->save();
                    $this->caption = 'White moves!';
                    // $this->chat->match->state->resetGame();
                    $this->sendImage();
                } else {
                    $this->reply('You cannot play a game with yourself');
                }
            }
        }

        if(preg_match('/^[KNPBRQ][a-h]?[1-8]?x?[a-h][1-8]=?[KNPBRQ]?$/', $this->text) === 1 || $this->text == 'O-O' || $this->text == 'O-O-O'){
            if($this->chat->match->turn == $this->user_id) {
                $matchResult = $this->chat->match->state->gameOver();
                error_log($matchResult);
                if($matchResult !== false) {
                    if($matchResult === 'D'){
                        $this->caption = 'The match ended. It\'s draw';
                        $this->sendImage(true);
                    } else {
                        $winner = ($matchResult == 'W') ? 'White' : 'Black';
                        $this->caption = 'The match ended. ' . $winner . ' wins!';
                        $this->sendImage(true);
                    }
                } else {
                    $this->move();
                }
            } else {
                $this->reply('It\'s not your turn');
            }
        } else {
            return false;
        }
    }

    public function sendImage($revert = false){
        // error_log($this->chat->userIdB);
        // error_log($this->user_id);
        if($revert){
            if($this->user_id == $this->chat->userIdB){
                $this->rotate = true;
            }
        } else {
            if($this->user_id != $this->chat->userIdB){
                $this->rotate = true;
            }
        }

        $boardImage = $this->chat->match->getBoardImage();
        // error_log(serialize($this->rotate));

        ob_start();
        $boardImage->outputImage($this->rotate);
        $content_file = ob_get_contents();
        ob_end_clean();

        $fields = [
            'chat_id'=> $this->chat_id,
            'caption' => $this->caption,
            // 'disable_web_page_preview'=> true,
            // 'reply_to_message_id': $message_id,
        ];
        srand((double)microtime()*100000);
        $boundary = "----------".substr(md5(rand(0,32000)),0,10);

        // Build the header
        $header = "Content-type: multipart/form-data, boundary=$boundary\r\n";

        $data = '';
        foreach($fields as $key => $value) {
            if($value){
                $data .= "--$boundary\r\n";
                $data .= 'Content-Disposition: form-data; name="' . $key . '"' . "\r\n\r\n";
                $data .= $value . "\r\n";
            }
        }
        $data .= "--$boundary\r\n";
        $data .="Content-Disposition: form-data; name=\"photo\"; filename=\"image.jpeg\"\r\n";
        $data .= "Content-Type: image/jpeg\r\n\r\n";
        $data .= $content_file."\r\n";
        $data .="--$boundary--\r\n\r\n";
        $header .= "Content-length: " . strlen($data) . "\r\n\r\n";

        $r = $this->do_post_request(BASE_URL . 'sendPhoto', $data, $header);
        error_log($r);
        exit;
    }

    public function sendGif()
    {
        // ob_start();
        $content_file = $this->chat->match->generateGif();
        // $content_file = ob_get_contents();
        // ob_end_clean();

        $fields = [
            'chat_id'=> $this->chat_id,
            'caption' => $this->caption,
            // 'disable_web_page_preview'=> true,
            // 'reply_to_message_id': $message_id,
        ];
        srand((double)microtime()*100000);
        $boundary = "----------".substr(md5(rand(0,32000)),0,10);

        // Build the header
        $header = "Content-type: multipart/form-data, boundary=$boundary\r\n";

        $data = '';
        foreach($fields as $key => $value) {
            if($value){
                $data .= "--$boundary\r\n";
                $data .= 'Content-Disposition: form-data; name="' . $key . '"' . "\r\n\r\n";
                $data .= $value . "\r\n";
            }
        }
        $data .= "--$boundary\r\n";
        $data .="Content-Disposition: form-data; name=\"document\"; filename=\"image.gif\"\r\n";
        $data .= "Content-Type: image/gif\r\n\r\n";
        $data .= $content_file."\r\n";
        $data .="--$boundary--\r\n\r\n";
        $header .= "Content-length: " . strlen($data) . "\r\n\r\n";

        // error_log($data);
        try {
            $r = $this->do_post_request(BASE_URL . 'sendDocument', $data, $header);
            error_log($r);
        } catch(Exception $e) {
            error_log($e->getMessage());
        }

        exit;
    }

    public function do_post_request($url, $data, $optional_headers = null)
    {
        $params = array('http' => array(
          'method' => 'POST',
          'content' => $data
        ));
        if ($optional_headers !== null) {
            $params['http']['header'] = $optional_headers;
        }
        $ctx = stream_context_create($params);
        $fp = @fopen($url, 'rb', false, $ctx);
        if (!$fp) {
            throw new Exception("Problem with $url, $php_errormsg");
        }
        $response = @stream_get_contents($fp);
        if ($response === false) {
            throw new Exception("Problem reading data from $url, $php_errormsg");
        }
        return $response;
    }

    public function executeCommand()
    {
        if($this->chatType != 'group') {
            $this->reply('You need to be in a chat group to play');
        }

        $command = strtolower(substr($this->text, 1));

        switch ($command) {
            case 'newmatch@phpchessbot':
            case 'newmatch':
                if($this->chat->isGameOn()){
                    $this->reply('No new matches while still playing.');
                } else {
                    $this->newMatch();
                }
                break;

            case 'endmatch@phpchessbot':
            case 'endmatch':
                if($this->chat->isGameOn()){
                    if($this->user_id == $this->chat->userIdA || $this->user_id == $this->chat->userIdB){
                        $this->endMatch();
                    } else {
                        $this->reply('Only the two people playing can end their match. Come on. Enjoy the match!');
                    }
                } else {
                    $this->reply('No active matches.');
                }
                break;

            case 'resend@phpchessbot':
            case 'resend':
                $this->sendLastImage();
                break;

            case 'help@phpchessbot':
            case 'help':
                $this->help();
                break;

            case 'undo@phpchessbot':
            case 'undo':
                if(!$this->chat->match->state->gameOver()){
                    if($this->chat->match->turn == $this->user_id) {
                        $this->chat->match->undo();
                        $this->chat->match->turn = $this->chat->getOtherUser($this->user_id);
                        $this->chat->match->save();
                        $this->caption = 'Done!';
                        $this->sendImage();
                    } else {
                        $this->reply('Ask the other person to undo your move');
                    }
                } else {
                    $this->reply('The match has ended. Nobody wants to undo a move, here.');
                }
                break;

            case 'history':
            case 'history@phpchessbot':
                $history = $this->chat->match->getHistory();
                if($history) {
                    $this->reply($history);
                } else {
                    $this->reply('No history. Sorry!');
                }
                break;

            case 'replay':
            case 'replay@phpchessbot':
                if($this->user_id !== 6838744) {
                    $this->sendGif();
                }
                break;
        }
    }

    public function newMatch(){

        $this->chat->userIdA = $this->user_id;
        $this->chat->active = true;
        $this->chat->save();
        $this->chat->match->save();
        $this->reply('New match started. Who wants to play?');
    }

    public function endMatch()
    {
        if($this->user_id !== 6838744) {
            $this->chat->match->delete();
            $this->chat->delete();
            $this->reply('Match deleted');
        } else {
            $this->reply('Sei stato un cattivo bambino.');
        }
    }

    public function move()
    {
        $validMove = $this->chat->match->move($this->text);
        if($validMove === true) {
            $this->chat->match->turn = $this->chat->getOtherUser($this->user_id);
            $this->chat->match->save();
            if($this->chat->match->state->inCheck('W') || $this->chat->match->state->inCheck('B')) {
                $this->caption = "Check!";
            }
            if($this->chat->match->state->gameOver() === 'B'){
                $this->caption = "Checkmate! Black wins.";
            }
            if($this->chat->match->state->gameOver() === 'W'){
                $this->caption = "Checkmate! White wins.";
            }
            if($this->chat->match->state->gameOver() === 'D'){
                $this->caption = "It's a draw";
            }
            $this->sendImage();
        } else {
            $this->reply($validMove->message);
        }
    }

    public function sendLastImage()
    {
        $this->caption = 'This is the current board status';
        $this->sendImage(true);
    }

    public function help()
    {
        $this->reply("Start a match with /newmatch command. The other player must say \"me\" or \"daje\" to let the match begin.\n\nUse standard algebraic notation https://en.wikipedia.org/wiki/Algebraic_notation_(chess) to move a piece after you start a match.\n\nPieces are:\n - Pawn (P);\n - Bishop (B);\n - King (K);\n - Queen (Q);\n - Rook (R);\n - Knight (N).\n\nThe vertical column (files) are labeled a through h. The  horizontal rows (ranks) are numbered 1 to 8. Identify the piece with a capital letter and the file with a lowercase letter. So, for example, to move a Pawn to e4, write Pe4.\n\nWhen the move is ambiguous write the letter representing the piece, start and destination. To move a Knight from d8 to e6 write Nd8e6.\n\nAn x should be placed between start and destination to indicate a capture. So, for example: Nd8xe6 is a valid move with disambiguation and capture.\n\nTo promote a piece write the letter representing the piece at the end of the move. So, for example Pa8Q gets you a new queen!\n\nRead the article on Wikipedia!");
    }
}

$h = new WebhookHandler;

if($h->get() === false) exit;
if(!$h->checkMsg()) exit;

// $validMove = $match->move($h->text);

// if($validMove === true) {
    // $match->save();
    // $boardImage = $match->getBoardImage($chess);
    // $h->sendImage($image_data);
// } else {
    // $h->reply($validMove->message);
    // And cache it on the filesystem
    // $filePath = BASE_DIR . "/board/{$posImage}";
    // $boardImage->saveImage($filePath);
    // $f = file_get_contents($filePath);
    // error_log($f);

// }
