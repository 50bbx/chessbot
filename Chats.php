<?php
require_once 'medoo.php';
require_once 'Matches.php';

class Chats
{
    public function __construct($chatId)
    {
        $this->database = new medoo([
            'database_type' => 'mysql',
            'database_name' => 'chessbot',
            'server' => '10.130.63.101:3306',
            'username' => 'adminBqHui4Z',
            'password' => 'xlBvuKlBCz1E',
            'charset' => 'utf8'
        ]);

        $result = $this->database->select('chats', '*', [
            "chat_id[=]" => $chatId
        ]);

        if(count($result) == 1){
            // error_log('Un solo risultato');
            $this->id = $result[0]['id'];
            $this->chatId = $result[0]['chat_id'];
            $this->userIdA = $result[0]['user_id_a'];
            $this->userIdB = $result[0]['user_id_b'];
            $this->active = $result[0]['active'];
        } else {
            $this->chatId = $chatId;
            $this->active = false;
        }
        $this->match = new Matches($this->chatId);
    }

    public function save()
    {
        if(isset($this->id)) {
            $this->database->update('chats', [
                'chat_id' => $this->chatId,
                'user_id_a' => $this->userIdA,
                'user_id_b' => $this->userIdB,
                'active' => $this->active,
            ], [
                "id" => $this->id
            ]);
        } else {
            $this->database->insert('chats', [
                'chat_id' => $this->chatId,
                'user_id_a' => $this->userIdA,
                'active' => $this->active,
            ]);
        }
    }

    public function delete()
    {
        $this->database->delete('chats', [
            'chat_id[=]' => $this->chatId
        ]);
    }

    public function isActive()
    {
        if(isset($this->active)) {
            return $this->active;
        }
        return  false;
    }

    public function isGameOn()
    {
        if(!isset($this->userIdA)) return false;
        if(!isset($this->userIdB)) return false;

        return true;
    }

    public function getOtherUser($userId) {
        if($userId == $this->userIdA) return $this->userIdB;

        return $this->userIdA;
    }
}